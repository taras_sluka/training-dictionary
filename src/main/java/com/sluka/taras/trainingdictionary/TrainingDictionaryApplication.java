package com.sluka.taras.trainingdictionary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingDictionaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingDictionaryApplication.class, args);
	}
}
